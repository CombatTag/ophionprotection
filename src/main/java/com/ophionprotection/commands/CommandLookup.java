package com.ophionprotection.commands;

import com.google.common.collect.ImmutableSet;
import com.ophionprotection.OphionProtection;
import com.ophionprotection.config.ConfigPaths;
import com.ophionprotection.config.IPData;
import com.ophionprotection.config.UserData;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Derek on 7/30/2014.
 */
public class CommandLookup extends Command implements TabExecutor {

    private final List<String> arguments = new ArrayList<String>() {
        {
            add("add");
            add("ip");
            add("list");
            add("player");
            add("remove");
            add("ultrasearch");
            add("version");
        }
    };

    private String PREFIX = "\u00bb ";

    public CommandLookup() {
        super("lookup", "ophion.staff", "lk");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

        if (commandSender == OphionProtection.p.getProxy().getConsole()) {
            PREFIX = "> ";
        } else {
            PREFIX = "» ";
        }
        IPData ipData;

        switch (strings.length) {
            case 1:
                switch (strings[0].toLowerCase()) {
                    case "list":
                    case "l":
                        OphionProtection.p.getProxy().getPluginManager().dispatchCommand(commandSender, "lookup list 1");
                        return;
                    case "version":
                    case "v":
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Ophion Protection version ").color(ChatColor.RED).append(OphionProtection.p.getDescription().getVersion()).color(ChatColor.GRAY).append(".").color(ChatColor.RED).create());
                        return;
                    default:
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid command format").bold(true).color(ChatColor.RED).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <add|a> ").color(ChatColor.RED).append("<IP> <MESSAGE>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup ip ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <list|l>").color(ChatColor.RED).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <player|p> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <remove|r> ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <ultrasearch|us> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <version|v> ").color(ChatColor.RED).create());
                }
                return;
            case 2:
                ipData = OphionProtection.p.getConfigManager().getIPData(strings[1]);
                switch (strings[0].toLowerCase()) {
                    case "remove":
                    case "r":
                        if (ipData.getUsernames().size() == 0) {
                            UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(strings[1]);

                            if (userData == null) {
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("No one has join the server with the name or IP of ").color(ChatColor.RED).append(strings[1]).color(ChatColor.GRAY).append(".").color(ChatColor.RED).create());
                                return;
                            }

                            userData.setReason("");
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Removed the player ").color(ChatColor.RED).append(strings[1]).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + strings[1])).color(ChatColor.GRAY).append(" from the caution list.").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).create());
                            OphionProtection.p.getConfigManager().saveUserData(userData);
                            return;
                        }

                        ipData.setReason("");
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Removed the IP ").color(ChatColor.RED).append(strings[1]).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, ipData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + strings[1])).color(ChatColor.GRAY).append(" from the caution list.").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).create());
                        OphionProtection.p.getConfigManager().saveIPData(ipData);
                        return;
                    case "ip":
                        OphionProtection.p.getProxy().getPluginManager().dispatchCommand(commandSender, "lookup ip " + ipData.getIP() + " 1");
                        return;
                    case "list":
                    case "l":
                        int page;
                        try {
                            page = Integer.parseInt(strings[1]);
                        } catch (NumberFormatException e) {
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid number ").color(ChatColor.RED).append(strings[1]).color(ChatColor.GRAY).append("\".").color(ChatColor.RED).create());
                            return;
                        }
                        if (page < 1) {
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("The page number must be greater than 0.").color(ChatColor.RED).create());
                            return;
                        }
                        int pageSize = OphionProtection.p.getConfiguration().getInt(ConfigPaths.PAGE_SIZE);
                        int index = (page * pageSize) - pageSize;

                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_GRAY).append("IP Cautions").bold(true).color(ChatColor.RED).create());

                        File ipConfigFile = new File(OphionProtection.p.getIPConfigFile() + File.separator);
                        File[] ipDataFiles = ipConfigFile.listFiles();

                        HashMap<String, ArrayList<String>> onlineIPs = new HashMap<>();
                        HashMap<String, ArrayList<String>> onlinePlayers = new HashMap<>();
                        for (Map.Entry<String, UUID> playerIndex : OphionProtection.p.getConfigManager().getPlayersIndex().entrySet()) {
                            UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(playerIndex.getKey());

                            if (!userData.getReason().equals("")) {
                                onlinePlayers.put(playerIndex.getKey(), new ArrayList<>(Arrays.asList(new String[]{"OFFLINE"})));
                            }
                        }
                        for (File file : ipDataFiles) {
                            IPData cautionData = OphionProtection.p.getConfigManager().getIPData(file.getName().replace(".yml", ""));

                            if (!cautionData.getReason().equals("")) {
                                ArrayList<String> currentPlayersOnline = new ArrayList<>();
                                for (String string : cautionData.getUsernames()) {
                                    if (OphionProtection.p.getProxy().getPlayers().toString().contains(string)) {
                                        currentPlayersOnline.add(string);
                                    }
                                }
                                onlineIPs.put(cautionData.getIP(), currentPlayersOnline);
                            }
                        }

                        int maxPage = (int) Math.round(onlineIPs.keySet().size() / pageSize + 0.5);
                        if (page > maxPage) {
                            page = maxPage;
                            index = (page * pageSize) - pageSize;
                        }

                        commandSender.sendMessage(new ComponentBuilder("  ").append(PREFIX).color(ChatColor.DARK_GREEN).append("Online").color(ChatColor.RED).create());
                        ArrayList<String> playersToRemove = new ArrayList<>();
                        ArrayList<String> ipsToRemove = new ArrayList<>();
                        for (Map.Entry<String, ArrayList<String>> entry : onlinePlayers.entrySet()) {
                            if (OphionProtection.p.getProxy().getPlayers().toString().contains(entry.getKey())) {
                                playersToRemove.add(entry.getKey());
                                UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(entry.getKey());
                                if (userData != null) {
                                    commandSender.sendMessage(new ComponentBuilder("    ").append(PREFIX).color(ChatColor.DARK_GRAY).append(userData.getLastUsername()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + userData.getLastUsername())).color(ChatColor.GRAY).append(" - ").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).append(userData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(userData.getLastUsername()).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + userData.getLastUsername() + " ")).color(ChatColor.GRAY).create());
                                    commandSender.sendMessage(new ComponentBuilder("      ").append(PREFIX).color(ChatColor.DARK_GRAY).append(OphionProtection.p.getProxy().getPlayer(userData.getLastUsername()).getServer().getInfo().getName()).color(ChatColor.GRAY).create());
                                }
                            }
                        }
                        for (Map.Entry<String, ArrayList<String>> entry : onlineIPs.entrySet()) {
                            if (!entry.getValue().isEmpty()) {
                                ipsToRemove.add(entry.getKey());
                                IPData cautionData = OphionProtection.p.getConfigManager().getIPData(entry.getKey());
                                commandSender.sendMessage(new ComponentBuilder("    ").append(PREFIX).color(ChatColor.DARK_GRAY).append(cautionData.getIP()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, cautionData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + cautionData.getIP())).color(ChatColor.GRAY).append(" - ").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).append(cautionData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(cautionData.getIP()).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + cautionData.getIP() + " ")).color(ChatColor.GRAY).create());
                                for (String currentOnline : entry.getValue()) {
                                    UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(currentOnline);
                                    if (userData != null) {
                                        commandSender.sendMessage(new ComponentBuilder("      ").append(PREFIX).color(ChatColor.DARK_GRAY).append(currentOnline).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + currentOnline)).color(ChatColor.GRAY).append(" - ").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).append(OphionProtection.p.getProxy().getPlayer(currentOnline).getServer().getInfo().getName()).color(ChatColor.GRAY).create());
                                    }
                                }
                            }
                        }
                        for (String string : playersToRemove) {
                            onlinePlayers.remove(string);
                        }
                        for (String string : ipsToRemove) {
                            onlineIPs.remove(string);
                        }
                        HashMap<String, ArrayList<String>> offlineCautions = new HashMap<>(onlinePlayers);
                        offlineCautions.putAll(onlineIPs);
                        commandSender.sendMessage(new ComponentBuilder("  ").append(PREFIX).color(ChatColor.DARK_RED).append("Offline").color(ChatColor.RED).create());
                        for (int i = index; i < index + Math.min(offlineCautions.keySet().size() - index, pageSize); i++) {
                            String key = offlineCautions.keySet().toArray(new String[offlineCautions.keySet().size()])[i];
                            if (offlineCautions.get(key).isEmpty()) {
                                IPData cautionData = OphionProtection.p.getConfigManager().getIPData(key);
                                commandSender.sendMessage(new ComponentBuilder("    ").append(PREFIX).color(ChatColor.DARK_GRAY).append(cautionData.getIP()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, cautionData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + cautionData.getIP())).color(ChatColor.GRAY).append(" - ").color(ChatColor.RED).append(cautionData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(cautionData.getIP()).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + cautionData.getIP() + " ")).color(ChatColor.GRAY).create());
                            } else {
                                UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(key);
                                commandSender.sendMessage(new ComponentBuilder("    ").append(PREFIX).color(ChatColor.DARK_GRAY).append(userData.getLastUsername()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + userData.getLastUsername())).color(ChatColor.GRAY).append(" - ").color(ChatColor.RED).append(userData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(userData.getLastUsername()).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + userData.getLastUsername() + " ")).color(ChatColor.GRAY).create());
                            }

                        }
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Previous").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup list " + (page - 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup list " + (page - 1))).append(" Page " + page + "/" + maxPage + " ").color(ChatColor.RED).append("Next").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup list " + (page + 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup list " + (page + 1))).create());
                        return;
                    case "player":
                    case "p":
                        OphionProtection.p.getProxy().getPluginManager().dispatchCommand(commandSender, "lookup player " + strings[1] + " 1");
                        return;
                    case "ultrasearch":
                    case "us":
                        UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(strings[1]);

                        if (userData == null) {
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append(strings[1]).color(ChatColor.GRAY).append(" has never joined the server.").color(ChatColor.RED).create());
                            return;
                        }
                        commandSender.sendMessage(new ComponentBuilder(userData.getLastUsername()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + userData.getLastUsername())).color(ChatColor.GRAY).bold(true).append(" Ultra Search").bold(true).color(ChatColor.RED).create());
                        ArrayList<String> players = new ArrayList<>();
                        for (String string : userData.getPreviousIPs()) {
                            IPData previousIP = OphionProtection.p.getConfigManager().getIPData(string);
                            for (String username : previousIP.getUsernames()) {
                                if (!players.contains(username)) {
                                    players.add(username);
                                }
                            }
                        }
                        Collections.sort(players);
                        for (String username : players) {
                            commandSender.sendMessage(new ComponentBuilder("  ").append(PREFIX).color(ChatColor.DARK_GRAY).append(username).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + username)).color(ChatColor.GRAY).create());
                        }
                        return;
                    default:
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid command format").bold(true).color(ChatColor.RED).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <add|a> ").color(ChatColor.RED).append("<IP> <MESSAGE>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup ip ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <list|l>").color(ChatColor.RED).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <player|p> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <remove|r> ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <ultrasearch|us> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <version|v> ").color(ChatColor.RED).create());
                }
                return;
            case 3:
                if (!strings[0].equals("add") || strings[0].equals("a")) {
                    ipData = OphionProtection.p.getConfigManager().getIPData(strings[1]);
                    int page;
                    try {
                        page = Integer.parseInt(strings[2]);
                    } catch (NumberFormatException e) {
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid number ").color(ChatColor.RED).append(strings[2]).color(ChatColor.GRAY).append("\".").color(ChatColor.RED).create());
                        return;
                    }
                    if (page < 1) {
                        commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("The page number must be greater than 0.").color(ChatColor.RED).create());
                        return;
                    }
                    int pageSize = OphionProtection.p.getConfiguration().getInt(ConfigPaths.PAGE_SIZE);
                    int index = (page * pageSize) - pageSize;
                    int maxPage;
                    switch (strings[0].toLowerCase()) {
                        case "ip":
                            maxPage = (int) Math.round(ipData.getUsernames().size() / pageSize + 0.5);
                            if (page > maxPage) {
                                page = maxPage;
                                index = (page * pageSize) - pageSize;
                            }
                            if (ipData.getUsernames().size() == 0) {
                                commandSender.sendMessage(new ComponentBuilder(ChatColor.DARK_RED + PREFIX).color(ChatColor.DARK_RED).append("No one has joined the server with the IP of ").color(ChatColor.RED).append(strings[2]).color(ChatColor.GRAY).append(".").color(ChatColor.RED).create());
                            } else {
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append(strings[1] + "'s").bold(true).color(ChatColor.GRAY).append(" info").bold(true).color(ChatColor.RED).create());
                                for (int i = index; i < index + Math.min(ipData.getUsernames().size() - index, pageSize); i++) {
                                    String string = ipData.getUsernames().get(i);
                                    UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(string);
                                    commandSender.sendMessage(new ComponentBuilder(string).color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + string)).create());
                                }
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Previous").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup ip " + ipData.getIP() + " " + (page - 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + ipData.getIP() + " " + (page - 1))).append(" Page " + page + "/" + maxPage + " ").color(ChatColor.RED).append("Next").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup ip " + ipData.getIP() + " " + (page + 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + ipData.getIP() + " " + (page + 1))).create());
                            }
                            return;
                        case "player":
                        case "p":
                            UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(strings[1]);

                            if (userData == null) {
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append(strings[1]).color(ChatColor.GRAY).append(" has never joined the server.").color(ChatColor.RED).create());
                            } else {
                                maxPage = (int) Math.round(userData.getPreviousUsernames().size() / pageSize + 0.5);
                                if (page > maxPage) {
                                    page = maxPage;
                                    index = (page * pageSize) - pageSize;
                                }
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append(strings[1] + "'s").bold(true).color(ChatColor.GRAY).append(" info").bold(true).color(ChatColor.RED).create());
                                Calendar lastLogin = userData.getLastLogin();
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Last Login: ").color(ChatColor.RED).append(userData.getLastUsername()).color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + userData.getLastUsername())).append(" - ").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, null)).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, null)).color(ChatColor.RED).append(new SimpleDateFormat("hh:mm a 'on' MMMM dd yyyy").format(lastLogin.getTime())).color(ChatColor.GRAY).create());
                                commandSender.sendMessage(new ComponentBuilder(ChatColor.DARK_RED + PREFIX).color(ChatColor.DARK_RED).append("UUID: ").color(ChatColor.RED).append(userData.getPlayerUUID().toString()).color(ChatColor.GRAY).create());
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Previous Usernames:").color(ChatColor.RED).create());
                                for (Calendar calendar : userData.getPreviousUsernames().keySet()) {
                                    String username = userData.getPreviousUsernames().get(calendar);
                                    commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append(username).color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + username)).append(" - ").color(ChatColor.RED).append(new SimpleDateFormat("hh:mm a 'on' MMMM dd yyyy").format(calendar.getTime())).color(ChatColor.GRAY).create());
                                }
                                page = Integer.parseInt(strings[2]);
                                index = (page * pageSize) - pageSize;
                                maxPage = (int) Math.round(userData.getPreviousIPs().size() / pageSize + 0.5);
                                if (page > maxPage) {
                                    page = maxPage;
                                    index = (page * pageSize) - pageSize;
                                }
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Previous IPs:").color(ChatColor.RED).create());
                                for (int i = index; i < index + Math.min(userData.getPreviousIPs().size() - index, pageSize); i++) {
                                    String string = userData.getPreviousIPs().get(i);
                                    IPData ipData1 = OphionProtection.p.getConfigManager().getIPData(string);
                                    commandSender.sendMessage(new ComponentBuilder(string).color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, ipData1.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + string)).create());
                                }
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Previous").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup player " + strings[1] + " " + (page - 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + strings[1] + " " + (page - 1))).append(" Page " + page + "/" + maxPage + " ").color(ChatColor.RED).append("Next").color(ChatColor.GRAY).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup player " + strings[1] + " " + (page + 1)).create())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + strings[1] + " " + (page + 1))).create());
                            }
                            return;
                        default:
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid command format").bold(true).color(ChatColor.RED).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <add|a> ").color(ChatColor.RED).append("<IP> <MESSAGE>").color(ChatColor.GRAY).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup ip ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <list|l>").color(ChatColor.RED).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <player|p> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <remove|r> ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <ultrasearch|us> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                            commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <version|v> ").color(ChatColor.RED).create());
                    }
                    return;
                }
            default:
                if (strings.length > 1) {
                    if (strings[0].equals("add") || strings[0].equals("a")) {
                        if (strings.length > 2) {
                            ipData = OphionProtection.p.getConfigManager().getIPData(strings[1]);
                            if (ipData.getUsernames().size() == 0) {
                                UserData userData = OphionProtection.p.getConfigManager().getUserDataByUserName(strings[1]);

                                if (userData == null) {
                                    commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("No one has join the server with the name or IP of ").color(ChatColor.RED).append(strings[1]).color(ChatColor.GRAY).append(".").color(ChatColor.RED).create());
                                    return;
                                }

                                StringBuilder stringBuilder = new StringBuilder();

                                for (int i = 2; strings.length > i; i++) {
                                    if (i != 2) {
                                        stringBuilder.append(" ");
                                    }
                                    stringBuilder.append(strings[i]);
                                }
                                userData.setReason(stringBuilder.toString());
                                OphionProtection.p.getConfigManager().saveUserData(userData);
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Message: \"").color(ChatColor.RED).append(stringBuilder.toString()).color(ChatColor.GRAY).append("\".").color(ChatColor.RED).create());
                            } else {
                                StringBuilder stringBuilder = new StringBuilder();

                                for (int i = 2; strings.length > i; i++) {
                                    if (i != 2) {
                                        stringBuilder.append(" ");
                                    }
                                    stringBuilder.append(strings[i]);
                                }
                                ipData.setReason(stringBuilder.toString());
                                OphionProtection.p.getConfigManager().saveIPData(ipData);
                                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Message: \"").color(ChatColor.RED).append(stringBuilder.toString()).color(ChatColor.GRAY).append("\".").color(ChatColor.RED).create());
                            }
                        }
                        return;
                    }
                }
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("Invalid command format").bold(true).color(ChatColor.RED).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <add|a> ").color(ChatColor.RED).append("<IP/PLAYER> <MESSAGE>").color(ChatColor.GRAY).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup ip ").color(ChatColor.RED).append("<IP>").color(ChatColor.GRAY).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <list|l>").color(ChatColor.RED).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <player|p> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <remove|r> ").color(ChatColor.RED).append("<IP/PLAYER>").color(ChatColor.GRAY).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <ultrasearch|us> ").color(ChatColor.RED).append("<PLAYER>").color(ChatColor.GRAY).create());
                commandSender.sendMessage(new ComponentBuilder(PREFIX).color(ChatColor.DARK_RED).append("/lookup <version|v> ").color(ChatColor.RED).create());
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2 || args.length == 0) {
            return ImmutableSet.of();
        }

        ArrayList<String> players = new ArrayList<>(OphionProtection.p.getConfigManager().getPlayersIndex().keySet());

        Set<String> matches = new HashSet<>();
        if (args.length == 1) {
            String search = args[0].toLowerCase();
            for (String argument : arguments) {
                if (argument.toLowerCase().startsWith(search) && !argument.toLowerCase().equals(search)) {
                    matches.add(argument);
                }
            }
        } else {
            String search = args[1].toLowerCase();

            if (args[0].toLowerCase().equals("player") || args[0].toLowerCase().equals("p") || args[0].toLowerCase().equals("ultrasearch") || args[0].toLowerCase().equals("us")) {
                for (String player : players) {
                    if (player.toLowerCase().startsWith(search) && !player.toLowerCase().equals(search)) {
                        matches.add(player);
                    }
                }
            } else {
                File ipConfigFile = new File(OphionProtection.p.getIPConfigFile() + File.separator);
                File[] ipDataFiles = ipConfigFile.listFiles();

                for (File file : ipDataFiles) {
                    if (file.getName().toLowerCase().replace(".yml", "").startsWith(search) && !file.getName().toLowerCase().replace(".yml", "").equals(search)) {
                        matches.add(file.getName().toLowerCase().replace(".yml", ""));
                    }
                }
            }
            if (args[0].toLowerCase().equals("add") || args[0].toLowerCase().equals("a") || args[0].toLowerCase().equals("remove") || args[0].toLowerCase().equals("r")) {
                for (String player : players) {
                    if (player.toLowerCase().startsWith(search) && !player.toLowerCase().equals(search)) {
                        matches.add(player);
                    }
                }
            }
        }
        return matches;
    }
}