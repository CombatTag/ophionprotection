package com.ophionprotection.config;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.ArrayList;

/**
 * Created by Derek on 7/30/2014.
 */
public class IPData {

    private ArrayList<String> usernames = new ArrayList<>();
    private String IP;
    private String reason;
    private String PREFIX = "\u00bb ";

    public IPData(ArrayList<String> usernames, String IP, String reason) {
        this.usernames = usernames;
        this.IP = IP;
        this.reason = reason;
    }

    public void addUsername(String username) {
        usernames.add(username);
    }

    public void removeUsernames(String username) {
        usernames.remove(username);
    }

    public ArrayList<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(ArrayList<String> usernames) {
        this.usernames = usernames;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BaseComponent[] getComponentInfo() {
        ComponentBuilder componentBuilder = new ComponentBuilder(IP).color(ChatColor.GRAY).bold(true);
        for (int i = 0; i < usernames.size(); i++) {
            componentBuilder.append("\n").bold(false);
            componentBuilder.append("  " + PREFIX).color(ChatColor.DARK_RED);
            if (i == 5) {
                componentBuilder.append("...").color(ChatColor.GRAY);
                break;
            }
            componentBuilder.append(usernames.get(i)).color(ChatColor.GRAY);
        }
        return componentBuilder.create();
    }
}
