package com.ophionprotection.config;

import com.ophionprotection.OphionProtection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Derek on 7/28/2014.
 */
public class ConfigManager {

    private HashMap<String, UUID> indexedPlayers = new HashMap<>();

    public ConfigManager() {
        indexConfig();
    }

    public UserData getUserData(UUID playerUUID) {
        UserData userData = null;

        try {
            File userFile = new File(OphionProtection.p.getDataFolder() + File.separator + "UUIDs", playerUUID.toString() + ".yml");

            if (userFile.exists()) {
                Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(userFile);

                String lastUsername = configuration.getString("lastUsername");
                Calendar lastLogin = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                lastLogin.set(configuration.getInt("lastLogin.year"), configuration.getInt("lastLogin.month"), configuration.getInt("lastLogin.day"), configuration.getInt("lastLogin.hour"), configuration.getInt("lastLogin.minute"), configuration.getInt("lastLogin.second"));
                HashMap<Calendar, String> previousUsernames = new HashMap<>();
                String stringUsernames = configuration.getString("previousUsernames");
                String reason = "";
                if (configuration.getString("reason") != null) {
                    reason = configuration.getString("reason");
                }
                if (!stringUsernames.equals("")) {
                    String[] cellSplit = stringUsernames.split("::");
                    for (String cell : cellSplit) {
                        List<String> keyAndValues = Arrays.asList(cell.split(":&:"));
                        Calendar key = null;
                        String value = null;
                        for (String keyOrValue : keyAndValues) {
                            if ((keyAndValues.indexOf(keyOrValue) + 1) % 2 == 0) {
                                value = keyOrValue;
                            } else {
                                String[] dataType = keyOrValue.split("&&");
                                Calendar tempCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

                                int year = Integer.parseInt(dataType[0]);
                                int month = Integer.parseInt(dataType[1]);
                                int day = Integer.parseInt(dataType[2]);
                                int hour = Integer.parseInt(dataType[3]);
                                int minute = Integer.parseInt(dataType[4]);
                                int second = Integer.parseInt(dataType[5]);

                                tempCalendar.set(year, month, day, hour, minute, second);
                                tempCalendar.setTimeZone(TimeZone.getTimeZone("GMT"));
                                key = tempCalendar;
                            }
                        }
                        previousUsernames.put(key, value);
                    }
                }
                ArrayList<String> previousIPs = new ArrayList();
                if (configuration.get("previousIPs") instanceof ArrayList) {
                    previousIPs = (ArrayList) configuration.get("previousIPs");
                }
                UUID configUUID = UUID.fromString((String) configuration.get("UUID"));
                userData = new UserData(lastUsername, lastLogin, previousUsernames, previousIPs, configUUID, reason);
            } else {
                userData = new UserData("", null, new HashMap<Calendar, String>(), new ArrayList<String>(), null, "");
            }
        } catch (IOException exception) {
            System.out.println("An IOException has occurred, player config could not be loaded.");
        }
        return userData;
    }

    public UserData getUserDataByUserName(String name) {
        ProxiedPlayer proxiedPlayer = OphionProtection.p.getProxy().getPlayer(name);

        if (proxiedPlayer == null) {
            for (Map.Entry<String, UUID> playerIndex : indexedPlayers.entrySet()) {
                if (playerIndex.getKey().equalsIgnoreCase(name)) {
                    return getUserData(playerIndex.getValue());
                }
            }
            return null;
        } else {
            return getUserData(proxiedPlayer.getUniqueId());
        }
    }

    public void saveUserData(UserData userData) {
        File userFile = new File(OphionProtection.p.getDataFolder() + File.separator + "UUIDs", userData.getPlayerUUID().toString() + ".yml");

        if (!userFile.exists()) {
            try {
                userFile.createNewFile();
            } catch (IOException exception) {
                System.out.println("A IOException has occurred, player config could not be loaded.");
            }
        }

        try {
            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(userFile);
            Calendar lastLogin = userData.getLastLogin();

            configuration.set("lastUsername", userData.getLastUsername());
            configuration.set("lastLogin.year", lastLogin.get(Calendar.YEAR));
            configuration.set("lastLogin.month", lastLogin.get(Calendar.MONTH));
            configuration.set("lastLogin.day", lastLogin.get(Calendar.DAY_OF_MONTH));
            configuration.set("lastLogin.hour", lastLogin.get(Calendar.HOUR_OF_DAY));
            configuration.set("lastLogin.minute", lastLogin.get(Calendar.MINUTE));
            configuration.set("lastLogin.second", lastLogin.get(Calendar.SECOND));

            StringBuilder stringBuilder = new StringBuilder("");

            for (Calendar calendar : userData.getPreviousUsernames().keySet()) {
                String username = userData.getPreviousUsernames().get(calendar);

                stringBuilder.append(calendar.get(Calendar.YEAR));
                stringBuilder.append("&&");
                stringBuilder.append(calendar.get(Calendar.MONTH));
                stringBuilder.append("&&");
                stringBuilder.append(calendar.get(Calendar.DAY_OF_MONTH));
                stringBuilder.append("&&");
                stringBuilder.append(calendar.get(Calendar.HOUR_OF_DAY));
                stringBuilder.append("&&");
                stringBuilder.append(calendar.get(Calendar.MINUTE));
                stringBuilder.append("&&");
                stringBuilder.append(calendar.get(Calendar.SECOND));
                stringBuilder.append(":&:");
                stringBuilder.append(username);
                stringBuilder.append("::");
            }

            configuration.set("previousUsernames", stringBuilder.toString());
            configuration.set("previousIPs", userData.getPreviousIPs());
            configuration.set("UUID", userData.getPlayerUUID().toString());
            configuration.set("reason", userData.getReason());

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, userFile);
        } catch (IOException exception) {
            System.out.println("A IOException has occurred, player config could not be loaded.");
        }
    }

    public IPData getIPData(String IP) {
        IPData ipData = null;

        try {
            File ipFile = new File(OphionProtection.p.getIPConfigFile(), IP + ".yml");

            if (ipFile.exists()) {
                Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(ipFile);

                ArrayList<String> usernames = new ArrayList();
                if (configuration.getStringList("usernames") instanceof ArrayList) {
                    usernames = (ArrayList) configuration.getStringList("usernames");
                }
                String reason = configuration.getString("reason");

                ipData = new IPData(usernames, IP, reason);
            } else {
                ipData = new IPData(new ArrayList<String>(), IP, "");
            }
        } catch (IOException exception) {
            System.out.println("A IOException has occurred, player config could not be loaded.");
        }
        return ipData;
    }

    public void saveIPData(IPData ipData) {
        File ipFile = new File(OphionProtection.p.getIPConfigFile(), ipData.getIP() + ".yml");

        if (!ipFile.exists()) {
            try {
                ipFile.createNewFile();
            } catch (IOException exception) {
                System.out.println("A IOException has occurred, player config could not be loaded.");
            }
        }

        try {
            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(ipFile);

            configuration.set("usernames", ipData.getUsernames());
            configuration.set("reason", ipData.getReason());

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, ipFile);
        } catch (IOException exception) {
            System.out.println("A IOException has occurred, player config could not be loaded.");
        }
    }

    public void updatePlayerIndex(UUID playerUUID, String playerName) {
        indexedPlayers.put(playerName, playerUUID);
    }

    public HashMap<String, UUID> getPlayersIndex() {
        return indexedPlayers;
    }

    private void indexConfig() {
        File userDataFile = new File(OphionProtection.p.getUUIDConfigFile() + File.separator);
        if (!userDataFile.exists()) {
            OphionProtection.p.loadConfig();
        }
        File[] userDataFiles = userDataFile.listFiles();

        for (File file : userDataFiles) {
            UserData userData = this.getUserData(UUID.fromString(file.getName().replace(".yml", "")));
            indexedPlayers.put(userData.getLastUsername(), userData.getPlayerUUID());
        }
    }
}
