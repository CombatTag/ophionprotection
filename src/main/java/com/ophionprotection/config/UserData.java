package com.ophionprotection.config;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Derek on 7/28/2014.
 */
public class UserData {

    private String lastUsername;
    private Calendar lastLogin;
    private HashMap<Calendar, String> previousUsernames = new HashMap<>();
    private ArrayList<String> previousIPs = new ArrayList<>();
    private UUID playerUUID;
    private String reason;
    private String PREFIX = "\u00bb ";

    public UserData(String lastUsername, Calendar lastLogin, HashMap<Calendar, String> previousUsernames, ArrayList<String> previousIPs, UUID playerUUID, String reason) {
        this.lastUsername = lastUsername;
        this.lastLogin = lastLogin;
        this.previousUsernames = previousUsernames;
        this.previousIPs = previousIPs;
        this.playerUUID = playerUUID;
        this.reason = reason;
    }

    public void addPreviousUsername(Calendar date, String username) {
        previousUsernames.put(date, username);
    }

    public void removePreviousUsername(Calendar calendar) {
        previousUsernames.remove(calendar);
    }

    public void addPreviousIP(String IP) {
        previousIPs.add(IP);
    }

    public void removePreviousIP(String IP) {
        previousIPs.remove(IP);
    }

    public String getLastUsername() {
        return lastUsername;
    }

    public void setLastUsername(String username) {
        lastUsername = username;
    }

    public Calendar getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Calendar date) {
        lastLogin = date;
    }

    public HashMap<Calendar, String> getPreviousUsernames() {
        return previousUsernames;
    }

    public void setPreviousUsernames(HashMap<Calendar, String> usernames) {
        this.previousUsernames = usernames;
    }

    public ArrayList<String> getPreviousIPs() {
        return previousIPs;
    }

    public void setPreviousIPs(ArrayList<String> previousIPs) {
        this.previousIPs = previousIPs;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(UUID playerUUID) {
        this.playerUUID = playerUUID;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BaseComponent[] getComponentInfo() {
        ComponentBuilder componentBuilder = new ComponentBuilder(lastUsername).color(ChatColor.GRAY).bold(true);
        componentBuilder.append("\n").bold(false);
        componentBuilder.append("  " + PREFIX).color(ChatColor.DARK_RED);
        componentBuilder.append("Last Login: ").color(ChatColor.RED).append(new SimpleDateFormat("hh:mm a 'on' MMMM dd yyyy").format(lastLogin.getTime())).color(ChatColor.GRAY);
        componentBuilder.append("\n");
        componentBuilder.append("  " + PREFIX).color(ChatColor.DARK_RED);
        componentBuilder.append("UUID: ").color(ChatColor.RED).append(playerUUID.toString()).color(ChatColor.GRAY);
        componentBuilder.append("\n");
        componentBuilder.append("  " + PREFIX).color(ChatColor.DARK_RED);
        componentBuilder.append("Previous Usernames: ").color(ChatColor.RED);

        int i2 = 0;
        for (Calendar calendar : previousUsernames.keySet()) {
            String username = previousUsernames.get(calendar);
            componentBuilder.append("\n");
            componentBuilder.append("    " + PREFIX).color(ChatColor.DARK_RED);
            if (i2 == 3) {
                componentBuilder.append("...").color(ChatColor.GRAY);
                break;
            }
            componentBuilder.append(username).color(ChatColor.GRAY);
            i2++;
        }
        componentBuilder.append("\n");
        componentBuilder.append("  " + PREFIX).color(ChatColor.DARK_RED);
        componentBuilder.append("Previous IPs: ").color(ChatColor.RED);
        for (int i = 0; i < previousIPs.size(); i++) {
            componentBuilder.append("\n");
            componentBuilder.append("    " + PREFIX).color(ChatColor.DARK_RED);
            if (i == 3) {
                componentBuilder.append("...").color(ChatColor.GRAY);
                break;
            }
            componentBuilder.append(previousIPs.get(i)).color(ChatColor.GRAY);
        }

        return componentBuilder.create();
    }
}