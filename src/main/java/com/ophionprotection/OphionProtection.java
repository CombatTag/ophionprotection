package com.ophionprotection;

import com.google.common.io.ByteStreams;
import com.ophionprotection.commands.CommandLookup;
import com.ophionprotection.config.ConfigManager;
import com.ophionprotection.listener.CypherLoginListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;

/**
 * Created by Derek on 7/28/2014.
 */
public class OphionProtection extends Plugin {

    public static OphionProtection p;
    private ConfigManager configManager;
    private File UUIDConfigFile;
    private File IPConfigFile;
    private Configuration configuration;

    @Override
    public void onEnable() {
        p = this;
        UUIDConfigFile = new File(getDataFolder(), "UUIDs");
        IPConfigFile = new File(getDataFolder(), "IPs");
        loadConfig();
        getProxy().getPluginManager().registerListener(this, new CypherLoginListener());
        getProxy().getPluginManager().registerCommand(this, new CommandLookup());
    }

    public void loadConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        if (!UUIDConfigFile.exists()) {
            UUIDConfigFile.mkdirs();
        }
        if (!IPConfigFile.exists()) {
            IPConfigFile.mkdirs();
        }
        configManager = new ConfigManager();
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                InputStream inputStream = getResourceAsStream("config.yml");
                OutputStream outputStream = new FileOutputStream(configFile);
                ByteStreams.copy(inputStream, outputStream);
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to create configuration file", e);
        }
    }

    public File getUUIDConfigFile() {
        return UUIDConfigFile;
    }

    public File getIPConfigFile() {
        return IPConfigFile;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}