package com.ophionprotection.listener;

import com.ophionprotection.OphionProtection;
import com.ophionprotection.config.IPData;
import com.ophionprotection.config.UserData;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by Derek on 7/28/2014.
 */
public class CypherLoginListener implements Listener {

    @EventHandler
    public void onPostLogin(final PostLoginEvent event) {
        OphionProtection.p.getProxy().getScheduler().schedule(OphionProtection.p, new Runnable() {
            @Override
            public void run() {
                if (OphionProtection.p.getProxy().getPlayers().contains(event.getPlayer())) {
                    UserData userData = OphionProtection.p.getConfigManager().getUserData(event.getPlayer().getUniqueId());

                    if (!userData.getLastUsername().equals(event.getPlayer().getName())) {
                        if (!userData.getLastUsername().equals("")) {
                            userData.addPreviousUsername(userData.getLastLogin(), userData.getLastUsername());
                        }
                        userData.setLastUsername(event.getPlayer().getName());
                        OphionProtection.p.getConfigManager().updatePlayerIndex(userData.getPlayerUUID(), userData.getLastUsername());
                    }

                    String ipFull = event.getPlayer().getAddress().toString();
                    String[] ipSplit = ipFull.split("/");
                    String[] ipClean = ipSplit[1].split(":");

                    if (!userData.getPreviousIPs().contains(ipClean[0])) {
                        userData.addPreviousIP(ipClean[0]);
                    }

                    userData.setLastLogin(Calendar.getInstance(TimeZone.getTimeZone("GMT")));
                    userData.setPlayerUUID(event.getPlayer().getUniqueId());

                    OphionProtection.p.getConfigManager().updatePlayerIndex(userData.getPlayerUUID(), userData.getLastUsername());
                    OphionProtection.p.getConfigManager().saveUserData(userData);

                    IPData ipData = OphionProtection.p.getConfigManager().getIPData(ipClean[0]);

                    if (!ipData.getUsernames().contains(event.getPlayer().getName())) {
                        ipData.addUsername(event.getPlayer().getName());
                        OphionProtection.p.getConfigManager().saveIPData(ipData);
                    }

                    if (!ipData.getReason().equals("") || !userData.getReason().equals("")) {
                        for (ProxiedPlayer player : OphionProtection.p.getProxy().getPlayers()) {
                            if (player.hasPermission("ophion.staff")) {
                                player.sendMessage(new ComponentBuilder("\u00bb ").color(ChatColor.DARK_RED).append(event.getPlayer().getName()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, userData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup player " + event.getPlayer().getName())).color(ChatColor.GRAY).append(" has joined the server, alert triggered!").color(ChatColor.RED).create());
                                player.sendMessage(new ComponentBuilder("  \u00bb ").color(ChatColor.DARK_RED).append("IP: ").color(ChatColor.RED).append(ipClean[0]).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, ipData.getComponentInfo())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/lookup ip " + ipClean[0])).color(ChatColor.GRAY).create());
                                if (!userData.getReason().equals("")) {
                                    player.sendMessage(new ComponentBuilder("  \u00bb ").color(ChatColor.DARK_RED).append("Message: ").color(ChatColor.RED).append(userData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(userData.getLastUsername()).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + userData.getLastUsername() + " ")).color(ChatColor.GRAY).create());
                                }
                                if (!ipData.getReason().equals("")) {
                                    player.sendMessage(new ComponentBuilder("  \u00bb ").color(ChatColor.DARK_RED).append("Message: ").color(ChatColor.RED).append(ipData.getReason()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("/lookup add ").append(ipClean[0]).append(" ").create())).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/lookup add " + ipClean[0] + " ")).color(ChatColor.GRAY).create());
                                }
                            }
                        }
                    }
                }
            }
        }, 5, TimeUnit.SECONDS);
    }
}
